CREATE OR REPLACE PACKAGE TIBCO.PKG_SFDC_SEC_LOAD
IS
    TYPE office_cur IS REF CURSOR;
    TYPE close_office_object IS RECORD (
        p_batch_status VARCHAR2(10),
        p_officeID VARCHAR2(20),
        p_msg VARCHAR2(1000)
        );
    TYPE close_office_objects IS TABLE OF close_office_object;
    FUNCTION IsReady(P_ProcessName IN VARCHAR2) RETURN NUMBER;
    PROCEDURE GetOfficeBatch(p_batch_size IN NUMBER, o_officeList OUT office_cur);
    PROCEDURE CloseOfficeBatch(p_batch_status IN VARCHAR2, p_officeID IN VARCHAR2, p_msg IN VARCHAR2);
    PROCEDURE CloseOfficeBatchBulk(close_office_table IN close_office_objects);
END PKG_SFDC_SEC_LOAD;
/

CREATE OR REPLACE PACKAGE BODY TIBCO.PKG_SFDC_SEC_LOAD
AS
    /******************************************************************************
    NAME: PKG_SFDC_SEC_LOAD
    PURPOSE:
    REVISIONS:
    Ver Date Author Description
    --------- ---------- --------------- ------------------------------------
    1.0 5/24/2012 yanxu 1. Created this package body.
    ******************************************************************************/
    FUNCTION IsReady(P_ProcessName IN VARCHAR2)
        RETURN NUMBER
    IS
        l_isReady NUMBER := 1;
    BEGIN
        RETURN l_isReady;
    END;

    PROCEDURE GetOfficeBatch(p_batch_size IN NUMBER, o_officeList OUT office_cur)
    IS
        TmpVar NUMBER;
    BEGIN
        --TmpVar := Param1;
        OPEN o_officeList FOR
                'select
                OPERATING_COMPANY_CODE , OPERATING_COMPANY_NAME , REGION_CODE , REGION_NAME , OFFICE_CODE , OFFICE_NAME , OFFICE_ADDRESS1 , OFFICE_CITY , OFFICE_ZIP_CODE , OFFICE_STATE , OFFICE_COUNTRY , OFFICE_EFF_STATUS
                from tibco.sfdc_org_structure
                where ADB_L_DELIVERY_STATUS =''N''
                and rownum <='
                || p_batch_size;
    END;

    PROCEDURE CloseOfficeBatch(p_batch_status IN VARCHAR2,
                               p_officeID IN VARCHAR2,
                               p_msg IN VARCHAR2)
    IS
        l_status VARCHAR2(1) := 'E';
    BEGIN
        IF LOWER(p_batch_status) = 'true'
        THEN
            l_status := 'C';
        END IF;
        UPDATE tibco.sfdc_org_structure
        SET ADB_L_DELIVERY_STATUS = l_status,
            ADB_PROCESS           = SUBSTR(TRIM(p_msg), 1, 250)
        WHERE office_code = SUBSTR(p_officeID, INSTR(p_officeID, '-') + 1)
          AND UPPER(OPERATING_COMPANY_CODE) =
              UPPER(
                      SUBSTR(p_officeID, 1, INSTR(p_officeID, '-') - 1));
    END;

    PROCEDURE CloseOfficeBatchBulk(close_office_table IN close_office_objects)
    IS
        l_status VARCHAR2(1) := 'E';
    BEGIN
        FOR idx IN close_office_table.first .. close_office_table.last
            LOOP
                IF LOWER(close_office_table(idx).p_batch_status) = 'true'
                THEN
                    l_status := 'C';
                END IF;
                UPDATE tibco.sfdc_org_structure
                SET ADB_L_DELIVERY_STATUS = l_status,
                    ADB_PROCESS           = SUBSTR(TRIM(close_office_table(idx).p_msg), 1, 250)
                WHERE office_code =
                      SUBSTR(close_office_table(idx).p_officeID, INSTR(close_office_table(idx).p_officeID, '-') + 1)
                  AND UPPER(OPERATING_COMPANY_CODE) =
                      UPPER(SUBSTR(close_office_table(idx).p_officeID, 1,
                                   INSTR(close_office_table(idx).p_officeID, '-') - 1));
            END LOOP;
    END;
/* FUNCTION IsReady (P_ProcessName IN VARCHAR2)
 RETURN NUMBER
 IS
 l_isReady NUMBER := 1;
 BEGIN
 SELECT COUNT (1)
 INTO l_isReady
 FROM tibco.wf_process_status
 WHERE process_id = 'SFDC_SECURITY'
 AND completed_flag = 'Y'
 AND completed_datetime > SYSDATE - 1;
 RETURN l_isReady;
 END;*/


    /*   PROCEDURE GetEmployeeBatch(p_batch_size IN NUMBER,
           -- p_lock_id IN NUMBER,
                                  o_employeeList OUT employee_cur)
       IS
       BEGIN
           OPEN o_employeeList FOR
                   'select * from (select
                   EMPLID, OPCO, OFFICE_CODE, OFFICE_NAME, HR_DEPT_ID, APP_ROLE, APP_ROLE_NAME, ACTIVE_INACTIVE_FLG, LAST_NAME, FIRST_NAME, MIDDLE_NAME, DIVISION_SRC_KEY, DIVISION_NAME, EMAILID, DIRECT_LINE, CELL_NUMBER, ORIG_HIRE_DATE, TERMINATION_DATE, JOB_TITLE, SUPERVISOR_ID, LEVEL_NUM, LEVEL_DESCR, LAST_MODIFIED_DATE,JOB_CODE,JOB_CODE_DESC
                   from tibco.sfdc_app_users
                   where ADB_L_DELIVERY_STATUS =''N''
                   order by LEVEL_NUM
                   )'
                   || ' where rownum <='
                   || p_batch_size;
       END;
       PROCEDURE GetManagerBatch(p_batch_size IN NUMBER,
           -- p_lock_id IN NUMBER,
                                 o_employeeList OUT employee_cur)
       IS
       BEGIN
           OPEN o_employeeList FOR
                   'select
                   EMPLID, OPCO, OFFICE_CODE, OFFICE_NAME, APP_ROLE, APP_ROLE_NAME, ACTIVE_INACTIVE_FLG, LAST_NAME, FIRST_NAME, MIDDLE_NAME, DIVISION_SRC_KEY, DIVISION_NAME, EMAILID, DIRECT_LINE, CELL_NUMBER, ORIG_HIRE_DATE, TERMINATION_DATE, JOB_TITLE, SUPERVISOR_ID, LAST_MODIFIED_DATE
                   from tibco.sfdc_app_users
                   where LAST_MODIFIED_DATE > sysdate -1 and ADB_L_DELIVERY_STATUS =''N''
                   and (trim(SUPERVISOR_ID) is null or emplid in (select SUPERVISOR_ID from tibco.sfdc_app_users ) )
                   and rownum <='
                   || p_batch_size;
       END;*/

    /*  PROCEDURE CloseEmployeeBatch(p_batch_status IN VARCHAR2,
                                   p_employeeID IN VARCHAR2,
                                   p_msg IN VARCHAR2)
      IS
          l_status VARCHAR2(1) := 'E';
      BEGIN
          IF LOWER(p_batch_status) = 'true'
          THEN
              l_status := 'C';
          END IF;
          UPDATE tibco.sfdc_app_users
          SET ADB_L_DELIVERY_STATUS = l_status,
              ADB_PROCESS           = SUBSTR(TRIM(p_msg), 1, 250)
          WHERE EMPLID = p_EmployeeID;
      END;
      PROCEDURE CloseSecurityLoad
      IS
          l_err_emp_count PLS_INTEGER;
          l_err_office_count PLS_INTEGER;
      BEGIN
          SELECT COUNT(1) INTO l_err_emp_count
          FROM tibco.sfdc_app_users
          WHERE ADB_L_DELIVERY_STATUS IN ('E', 'N')
  -- AND LAST_MODIFIED_DATE > SYSDATE - 1
          ;
          SELECT COUNT(1) INTO l_err_office_count
          FROM tibco.sfdc_org_structure
          WHERE ADB_L_DELIVERY_STATUS IN ('E', 'N')
  -- AND LAST_MODIFIED_DATE > SYSDATE - 1
          ;
          IF (l_err_emp_count + l_err_emp_count) = 0
          THEN
              UPDATE tibco.wf_process_status
              SET completed_flag = 'C'
              WHERE process_id = 'SFDC_SECURITY'
                AND completed_flag = 'Y'
                AND completed_datetime > SYSDATE - 1;
          END IF;
      END;*/
END PKG_SFDC_SEC_LOAD;
